function Data = extractmeasurement( Data, fi )

% EXTRACTMEASUREMENT - Extract measurement from data struct
% NewData = extractmeasurement(Data,fi)
% Data .. data struct with posisions and fields
% fi .. vector of indices or logical values
% example: Data1 = extractmeasurement(Data,Data.u>0)
% See also: delmeasurement

if nargin<2, return; end
if min(fi)<=0, 
    fi = find(fi); 
end
fie = fieldnames( Data );
if isfield( Data, 'a' ),
    ndata = length( Data.a );
elseif isfield( Data, 's' ),
    ndata = length( Data.s ); 
else
    error('did not found a or s field!');
end
for i=1:length(fie),
   ff = getfield( Data, fie{i} ); 
   if ( min( size(ff) ) == 1 ) && ( length(ff) == ndata ),
       Data = setfield( Data, fie{i}, ff(fi) );
   end
end
