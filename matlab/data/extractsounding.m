function Data1=extractsounding(Data,pos,pos2)

% EXTRACTSOUNDING - Extract median sounding from 2d data set
% Data1 = extractsounding(Data)

if nargin>2, % pos2 given
    if pos2<pos, % apparently a width (can be given negatively)
        pos1 = pos-abs(pos2);
        pos2 = pos+abs(pos2);
    else
        pos1 = pos;
    end
else
    pos2 = 0;
end
[pos pos1 pos2]
am = Data.a - Data.m;
bm = Data.b - Data.m;
[unabm,II,JJ] = unique( [am bm], 'rows' );
medx = (Data.elec(Data.a,1)+Data.elec(Data.b,1)+...
        Data.elec(Data.m,1)+Data.elec(Data.n,1) ) / 4;
% medabmn = ( Data.a + Data.m + Data.b + Data.n ) / 4;
% medel = floor( size(Data.elec,1) / 2 );
Data1 = [];
Data1.elec = Data.elec;
Data1.r = zeros( length(II), 1 );
for i=1:length(II),
    fi = find( JJ==i ); % all
    if nargin>2, % min/max given
        fo = find((medx(fi)>=pos1)&(medx(fi)<=pos2));
        fi = fi(fo);
    elseif nargin>1, % one position
        [mi,fo] = min(abs(medx(fi)-pos));
        fi = fi(fo);
%         fprintf('%d: %f\n',i,medx(fi));
    end
    if ~isempty(fi),
        Data1.r(i) = median(Data.r(fi));
    %     [mi,fm]=min(abs(Data.a(fi)-median(Data.a(fi))));
    %     [mi,fm]=min(abs(medabmn(fi)-medel));
    %     fm=fm(1);
        fm = 1;
        Data1.a(i) = Data.a( fi(fm) );
        Data1.b(i) = Data.b( fi(fm) );
        Data1.m(i) = Data.m( fi(fm) ); 
        Data1.n(i) = Data.n( fi(fm) );
    end
end