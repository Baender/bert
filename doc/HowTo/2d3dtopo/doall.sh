#!/usr/bin/env bash

# Make the 2d files and the 3d file
python mk3ddata.py

# We are now doing inversion of the individual 2d files and the 3d file.
cd 2d
for i in `seq 1 7`
do
  mkdir -p $i
  cp p$i.dat $i/
  cd $i/
  bertNew2DTopo p$i.dat > bert.cfg
  echo PRIMDX=0.05 >> bert.cfg
  echo ZWEIGHT=0.2 >> bert.cfg
  echo LOWERBOUND=100 >> bert.cfg
  echo UPPERBOUND=2000 >> bert.cfg 
  bert bert.cfg all
  cp dcinv.result.vtk ../p$i.vtk
  bert bert.cfg save
  cd ..
done

cd ..
cd 3d
bertNew3dTopo alldata.dat > bert.cfg
bert bert.cfg all
cd ..

python transformvtk.py