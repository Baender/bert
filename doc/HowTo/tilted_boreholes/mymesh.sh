MESH=mesh/mesh
mkdir -p mesh
# definition of geometry
SEG=6 # number of segments
RAD=0.045 # radius of borehole
DRING=0.022 # thickness of electrode ring
XBOR=(0.121 0.983 1.733 0.435 1.257 0.059 0.950 1.835) # borehole x
YBOR=(0.764 0.775 0.754 0.367 0.499 0.058 0.021 0.027) # borehole y
PD=(0.03 0.052 0.045 0.025 0.047 0.05 0.072 0.052)     # depth of 1st electrode
DIST=(0.103 0.152 0.202 0.252 0.302 0.353 0.100)       # distance between electrodes
# rotation of boreholes by y axis
ROT=(0.041153223 0.03377094 0.03869036 0.020405331 0.01290251 0.041298818 0.08184799 0.013512691)
#create box of size SIZE
SIZE=100
polyCreateWorld -x $SIZE -y $SIZE -z $SIZE -m1 $MESH
polyScale -z 0.5 $MESH
# build borehole electrodes and add
polyCreateCube -H -Z -s $SEG cylC # create (closed) unit cylinder
polyTranslate -z -0.5 cylC # shift down to upper boundary at surface
polyScale -x $RAD -y $RAD cylC
polyCreateCube -O -H -Z -s $SEG cylO # create (open) unit cylinder
polyTranslate -z -0.5 cylO # shift down to upper boundary at surface
polyScale -x $RAD -y $RAD cylO
cp cylO.poly elec0.poly # unit electrode (open)
polyScale -z $DRING elec0 # right height
# run through all the boreholes
ELM=9999
for b in {0..7} #7
do    
    cp cylC.poly borehole.poly # uppermost piece
    polyScale -z ${PD[$b]} borehole
    # convert into STL (triangles) and back and reset hole marker to force triangular coplanar faces
    polyConvert -S borehole
    polyConvert -P borehole.stl
    polyAddVIP -H -x 0 -y 0 -z -0.01 borehole
    z=${PD[$b]}
    echo "borehole no $b"
    echo $z
    #run though all electrodes
    for e in {0..6}  #6
    do
        # shift electrode and add it to borehole
        ELM=$[ELM+1]
#        z=`echo $z 0.4|awk '{ print $1 + $2}'` #DEBUG!delete
        cp elec0.poly elec.poly
        polyAddVIP -B -m -$ELM elec
        polyTranslate -z -$z elec
        polyMerge borehole elec borehole
        z=`echo $z $DRING|awk '{ print $1 + $2}'`
        echo $z $DRING
        # shift electrode and add it to borehole
        cp cylC.poly cyl.poly
        DI=`echo ${DIST[$e]} $DRING|awk '{ print $1 - $2}'`
#        z=`echo $z 0.4|awk '{ print $1 + $2}'` #DEBUG!delete
        polyScale -z $DI cyl
        polyTranslate -z -$z cyl
        polyMerge borehole cyl borehole
        z=`echo $z $DI|awk '{ print $1 + $2}'`        
        echo $z $DI
    done # borehole pieces
    # rotate boreholes
    polyRotate -R -y ${ROT[$b]} borehole
    # set 6 z positions to zero
    head -n1 borehole.poly > newborehole.poly
    head -n3 borehole.poly |tail -n2|awk '{print $1 "\t" $2 "\t" $3 "\t" 0.00 "\t" $5}' >> newborehole.poly
    head -n6 borehole.poly |tail -n3 >> newborehole.poly
    head -n7 borehole.poly |tail -n1|awk '{print $1 "\t" $2 "\t" $3 "\t" 0.00 "\t" $5}' >> newborehole.poly
    head -n8 borehole.poly |tail -n1 >> newborehole.poly
    head -n9 borehole.poly |tail -n1|awk '{print $1 "\t" $2 "\t" $3 "\t" 0.00 "\t" $5}' >> newborehole.poly
    head -n10 borehole.poly |tail -n1 >> newborehole.poly
    head -n11 borehole.poly |tail -n1|awk '{print $1 "\t" $2 "\t" $3 "\t" 0.00 "\t" $5}' >> newborehole.poly
    head -n12 borehole.poly |tail -n1 >> newborehole.poly
    head -n13 borehole.poly |tail -n1|awk '{print $1 "\t" $2 "\t" $3 "\t" 0.00 "\t" $5}' >> newborehole.poly
    NL=`wc -l borehole.poly|cut -f1 -d" "`
    tail -n $[NL - 13] borehole.poly >> newborehole.poly    
    polyTranslate -x ${XBOR[$b]} -y ${YBOR[$b]} newborehole # translate to bh position
    polyMerge $MESH newborehole $MESH                       # merge with world
done # boreholes
polyConvert -V newborehole
# add surface electrodes and refine them
polyAddVIP -f surface_elec.xyz -m -99 $MESH
polyRefineVIPS -z -0.015 $MESH
polyConvert -V -o $MESH-poly $MESH
rm -f wrong.vtk # possible old wrong file
tetgen -d mesh/mesh
if [ -f mesh/mesh.1.node ]
then
    meshconvert -v -it -V -o wrong mesh/mesh.1
    echo "detected geometry problem! Look at wrong.vtk"
else
    tetgen -pazVACq1.15 $MESH # mesh PLC with quality factor 1.2 giving mesh.1.*
    meshconvert -vBDMV -it -o $MESH $MESH.1 # convert mesh to BMS and MESH and VTK
    meshconvert -vBD -p -o ${MESH}P $MESH.bms
fi
rm $MESH.1.*