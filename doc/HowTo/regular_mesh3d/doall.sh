SIZE=300
python mkreg3dmesh.py #creates para.bms/vtk and paraBoundary.poly
polyConvert -V paraBoundary
polyCreateWorld -x $SIZE -y $SIZE -z $SIZE world
polyScale -z 0.5 world
polyMerge world paraBoundary worldSurface
polyConvert -V worldSurface
polyAddVIP -x0 -y0 -z0 -H worldSurface
tetgen -pazVAC worldSurface
meshconvert -vBDM -it -o worldBoundary worldSurface.1
python extractmysurface.py # creates worldBoundary.poly
polyConvert -V worldBoundary
# merge world and para without check
polyMerge -N worldBoundary paraBoundary allBoundary
polyConvert -V allBoundary
# remove doubled Nodes by recounting
python readmypolyfile.py   # creates test.poly
polyConvert -V test
rm -f wrong.vtk right.vtk test.1.*
tetgen -d test
if [ -f test.1.face ]
then
    echo "Detected intersecting subfaces! See wrong.vtk"
    meshconvert -V -it -o wrong test.1
else
    tetgen -pazVACY -q 1.2 test
    meshconvert -VBDM -it -o right test.1
fi
python combinemymeshes.py