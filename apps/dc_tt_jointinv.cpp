/***************************************************************************
 *   Copyright (C) 2006-2014 by the resistivity.net development team       *
 *   Carsten R�cker carsten@resistivity.net                                *
 *   Thomas G�nther thomas@resistivity.net                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <bert.h>
#include <optionmap.h>
#include <ttdijkstramodelling.h>
#include <dcfemmodelling.h>
#include <inversion.h>

#include <string>
using namespace GIMLI;
using namespace BERT;

#define vcout if (verbose) std::cout
#define dcout if (debug) std::cout
#define DEBUG if (debug)

typedef SparseMapMatrix < double, size_t > SensMatTypeTT; // sparse map matrix
typedef RMatrix SensMatTypeDC; // full matrix

int main(int argc, char *argv []) {

    //** Initialisation of inversion options;
    bool lambdaOpt = false, isBlocky = false, isRobust = false, noCoupling = false;
    bool overrideError = false, createGradientModel = false, recalcDCJac = false;
    bool useDCFEMLib = false, singRemoval = false, loadSensMat = false, ttRefined = true; //,recalcJac = false;
    int maxIter = 10, verboseCount = 0;
    double lambdaTT = 100.0, lambdaDC = 100.0, zWeight = 1.0;
    double errPercTT = 0.1, errTime = 0.0, errPercDC = 3, errVolt = 100e-6, defaultCurrent = 100e-3;

    if (false) { // set true for synthetic model
        errTime = 0.0; errVolt = 0.0; errPercDC = 2.0; lambdaDC = 20.0; lambdaTT = 1000.0;
    }

    std::string paraMeshFilename(NOT_DEFINED), ttFileName(NOT_DEFINED), ttStartFileName(NOT_DEFINED);
    std::string dcStartFileName(NOT_DEFINED), mcFileName(NOT_DEFINED), primPotFileBody(NOT_DEFINED);
    std::string dcFileName, sensFileName("Sens.mat");

    OptionMap oMap;
    oMap.setDescription("Description. DC_Ra_Joininv - Joint inversion of DC resistivity and traveltime data.\n");
    oMap.addLastArg(dcFileName, "Data file");
    oMap.add(verboseCount,       "v" , "verbose", "Verbose mode (2 times for debug mode).");
    oMap.add(useDCFEMLib,        "1" , "UseDCFEMLib", "Use DCFEMLib1::BERT stuff (mesh, sens, primpot).");
    oMap.add(isBlocky   ,        "B" , "isBlocky", "Blocky model constraints.");
    oMap.add(isRobust,           "R" , "RobustData", "Robust (L1) data weighting.");
    oMap.add(recalcDCJac,        "J" , "RecalcDCJacobian", "Re-Calculate DC Sensitivity after each iteration.");
    oMap.add(overrideError ,     "E" , "overrideError", "Override error.");
    oMap.add(createGradientModel,"G" , "createGradientModel", "Create gradient start model.");
    oMap.add(noCoupling ,        "N" , "noCoupling", "Disable structural coupling.");
    oMap.add(loadSensMat ,       "L" , "loadSensMat", "Load sensitivity matrix.");
    oMap.add(lambdaOpt,          "O" , "OptimizeLambda", "Optimize model smoothness using L-curve.");
    oMap.add(lambdaDC,           "l:", "lambdaDC", "Regularization parameter lambda for DC.");
    oMap.add(lambdaTT,           "r:", "lambdaTT", "Regularization parameter lambda for TT.");
    oMap.add(errPercDC,          "e:", "errPercDC", "Error percentage for DC data.");
    oMap.add(errTime,            "t:", "errorTime", "Absolute error for first arrival times.");
    oMap.add(paraMeshFilename,   "p:", "paraMeshFile", "Parameter mesh file.");
    oMap.add(maxIter,            "i:", "iterations", "Maximum iteration number.");
    oMap.add(ttFileName,         "f:", "TTFileName", "Travel time file name.");
    oMap.add(dcStartFileName,    "a:", "DCStartFileName", "DC starting file name.");
    oMap.add(ttFileName,         "b:", "TTStartFileName", "TT starting file name.");
    oMap.add(mcFileName,         "c:", "MCFileName", "model control file name.");
    oMap.add(sensFileName,       "j:", "sensFileName", "sensitivity file name.");
    oMap.add(primPotFileBody,    "y:", "primaryPotFileBody", "File name (without .#) of primary potentials.");
    oMap.add(zWeight,            "z:", "zWeight", "z-weight (vertical constraint weight)[1=isotropic]");
    oMap.parse(argc, argv);

    bool verbose = (verboseCount > 0), debug = (verboseCount > 1);

    DEBUG verbose = true;
    if (verbose) setbuf(stdout, NULL);

    if (useDCFEMLib) {
        if (paraMeshFilename == NOT_DEFINED) paraMeshFilename = "mesh/mesh.bms"; //! use 1/2 mesh for inversion (background region + para region)
        if (sensFileName == "Sens.mat") sensFileName = "sensM/smatrix";
        if (primPotFileBody == NOT_DEFINED) primPotFileBody = "primaryPot/interpolated/potential.P"; //! catch .D (dipole files)
        vcout << "DCFEMLib mode: trying to use " << paraMeshFilename << " , " << sensFileName << " , " << primPotFileBody << std::endl;
        loadSensMat = true;
        singRemoval = true;
        if (lambdaDC <= 0.0 && (! lambdaOpt)) lambdaDC = 20.0; //! only if not set and not to optimize
        if (lambdaTT <= 0.0 && (! lambdaOpt)) lambdaTT = 20.0; //! only if not set and not to optimize
    }

    //** Initialise data container for DC and TT including error estimation;
    DataContainerERT dataDC(dcFileName);
    if (verbose) dataDC.showInfos();
    DataContainer dataTT;
    dataTT.registerSensorIndex("s");
    dataTT.registerSensorIndex("g");
    dataTT.load(ttFileName, true);
    if (verbose) dataTT.showInfos();

    /*! mesh initialization */
    Mesh paraMesh;
    if (paraMeshFilename != NOT_DEFINED) {
        paraMesh.load(paraMeshFilename);
        vcout << "ParaMesh:\t" << paraMesh << std::endl;
        DEBUG paraMesh.exportVTK("meshPara");
    } else {
        throwError(1, WHERE_AM_I + " no para mesh given.");
    }

    //!** initialize travel time inversion;
    TravelTimeDijkstraModelling fTT(paraMesh, dataTT);
    //!** set default behavior for BERT regions
    if (fTT.regionManager().regionCount() > 1){
        fTT.regionManager().regions()->begin()->second->setBackground(true);
    }
    fTT.createRefinedForwardMesh(ttRefined, false);

    RVector appSlowness (fTT.getApparentSlowness());
    vcout << "min/max apparent velocity = " << 1.0 / max(appSlowness)
          << "/" << 1.0 / min(appSlowness) << "m/s" << std::endl;

    Mesh paraDomain(fTT.regionManager().paraDomain());
    DEBUG paraDomain.save("meshParaDomain.bms");
    int nModel = paraDomain.cellCount();
    SensMatTypeTT STT;
    vcout << "model size = " << nModel << std::endl;
    RVector startModelTT(fTT.startModel());
    
    
    if (createGradientModel) {
        double smi = min(appSlowness) / 2.0;
        double sma = max(appSlowness) / 2.0;

        vcout << "Creating Gradient model ..." << std::endl;
        RVector zmid(nModel);
        int di = paraMesh.dim() - 1;
        for (size_t i = 0; i < paraDomain.cellCount() ; i++) {
            for (size_t j = 0; j < paraDomain.cell(i).nodeCount(); j++) {
                zmid[ i ] += paraDomain.cell(i).node(j).pos()[ di ];
            }
            zmid[ i ] /= double(paraDomain.cell(i).nodeCount());
        }
        double zmi = min(zmid);
        double zma = max(zmid);
        for (size_t i = 0; i < startModelTT.size(); i++) {
            startModelTT[i] = smi * exp((zmid[ i ] - zmi) / 
                                        (zma - zmi) * log(sma / smi));
        }
        DEBUG save(startModelTT, "startModel.vector");
    }
    dcout << "startModel-TT.size()=" << startModelTT.size() 
          << " min=" << min(startModelTT) 
          << " max=" << max(startModelTT) << std::endl;
    fTT.setStartModel(startModelTT);

    //!** initialize DC forward modelling operator
    
    DCSRMultiElectrodeModelling fDC(paraMesh, dataDC, verbose);
    fDC.setPrimaryPotFileBody(primPotFileBody);

    if (fileExist("dc.control")) fDC.regionManager().loadMap("dc.control");
    if (fileExist("tt.control")) fTT.regionManager().loadMap("tt.control");

    if (fDC.regionManager().regionCount() > 1){
        fDC.regionManager().regions()->begin()->second->setBackground(true);
    }
    fDC.createRefinedForwardMesh(true, false);

    fDC.createRefinedForwardMesh();
    vcout << "Secmesh:\t"; fDC.mesh()->showInfos();
    RVector startModelDC(fDC.regionManager().parameterCount(), median(dataDC("rhoa")));

    //** read and initialise start model (not implemented yet);
//  if (ttStartFileName != NOT_DEFINED){
//    load(startModelTT, ttStartFileName);
//    vcout << "read tt start model from file min=" << min(startModelTT) << " max=" << max(startModelTT) << std::endl;
//  }
//  RVector startModelDC(median(dataDC.rhoa()), nModel);
//  if (dcStartFileName != NOT_DEFINED){
//    load(startModelDC, dcStartFileName);
//    vcout << "read dc start model from file min=" << min(startModelDC) << " max=" << max(startmodelDC) << std::endl;
//  } else {
//  }
//      vcout << "Start model size=" << startModel.size() << " min/max=" << min(startModel) << "/" << max(startModel) << std::endl;

    //** create Jacobian matrices for both methods; not necessary??
//    vcout << "Creating Jacobian...";// << std::endl;
//    fTT.createJacobian(STT, startModelTT);
//    vcout << "ready with TT initialization" << std::endl;

    //** estimate data error for DC inversion if not defined;
    if (overrideError || !dataDC.allNonZero("err"))
        DCErrorEstimation(dataDC, errPercDC, errVolt, defaultCurrent, verbose);
    
    dcout << "DC Data error:" << " min = " << min(dataDC("err")) * 100 << "%" 
          << " max = " << max(dataDC("err")) * 100 << "%" << std::endl;

    //** estimate error for TT inversion
    if (overrideError || ! dataTT.allNonZero("err")) {
        vcout << "TT Estimate error: " << errPercTT 
              << "% + " << errTime * 1000 << "ms" << std::endl;
        dataTT.set("err", errTime / dataTT("t") + errPercTT / 100.0); // always relative error
    }
    vcout << "TT Data error:"
    << " min = " << min(dataTT("err")) * 100 << "%"
    << " max = " << max(dataTT("err")) * 100 << "%" << std::endl;

    //** initialise transformation classes for model and data (DC);
//    Trans < RVector > *transDataDC = new TransLog< RVector >();
//    Trans < RVector > *transModelDC = new TransLog< RVector >();
    RTrans transDataDC;
    RTrans transModelDC;
    //** initialise inversion and pass options;
    RInversion invDC(dataDC("rhoa"), fDC, transDataDC, transModelDC, verbose, false);
    invDC.setError(dataDC("err"));
    invDC.setLambda(lambdaDC);
    invDC.setMaxIter(1);
    invDC.setOptimizeLambda(false);
    invDC.setRecalcJacobian(recalcDCJac);
    invDC.setModel(startModelDC);
    SensMatTypeDC SDC;
    
    if (loadSensMat) {
        load(SDC, sensFileName);
        fDC.setJacobian(&SDC);
    }

    //** initialise TT transformations linear (travel time) and log (slowness);
    RTrans transDataTT;
//    RTransMult transDataTT(appSlowness / dataTT.t()); //** apparent slowness
    RTransLog transModelTT;
    //RTrans transModelTT; //** linear
    //** initialise inversion and pass options;
    RInversion invTT(dataTT("t"), fTT, transDataTT, transModelTT, verbose, false);
    invTT.setModel(startModelTT);
    invTT.setLambda(lambdaTT);
    fTT.setJacobian(& STT);
    invTT.setError(dataTT("err"));
    invTT.setMaxIter(1);
    invTT.setOptimizeLambda(false);

    int nBounds = fDC.constraintsRef().rows();
    fDC.regionManager().setZWeight(zWeight);
    fTT.regionManager().setZWeight(zWeight);
    RVector flatWeight = fDC.regionManager().createConstraintsWeight();

    //** read eventually model control file;
//  if (mcFileName != NOT_DEFINED){
//    RVector modelControl(mcFileName);
//    vcout << "read model constrol from file. min=" << min(modelControl) << " max=" << max(modelControl) << endl;
//    invTT.setMWeight(modelControl);
//    invDC.setMWeight(modelControl);
//  }

    //** create model vectors;
    RVector modelDC(nModel);
    RVector modelTT(nModel);
    //** perform inversion startup;
    vcout << "-------- DC inversion -------" << std::endl;
    modelDC = invDC.run();
    DEBUG save(modelDC, "modelDC_i.vector");
    vcout << "-------- TT inversion -------" << std::endl;
    modelTT = invTT.run();
    DEBUG save(modelTT, "modelTT_i.vector");

    RVector cWeight(nBounds);
    for (int iter = 0; iter < maxIter; iter++) {
        fTT.createJacobian(STT, modelTT);
        if (isRobust) {
            invDC.robustWeighting();
            invTT.robustWeighting();
        }
        //** structural coupling as for blocky model;
        if (! noCoupling) {
            cWeight = invDC.getIRLS();
//            cWeight = 1.0 / abs(invDC.roughness() + 0.1) + 0.1;
            DEBUG save(cWeight, "irlsDC.vec");
            invTT.setCWeight(cWeight * flatWeight);
            cWeight = invTT.getIRLS();
//            cWeight = 1.0 / abs(invTT.roughness() + 0.1) + 0.1;
            DEBUG save(cWeight, "irlsTT.vec");
            invDC.setCWeight(cWeight * flatWeight);
        }
        //** one inversion step;
        vcout << "-------- DC inversion -------" << std::endl;
        invDC.oneStep();
        vcout << "-------- TT inversion -------" << std::endl;
        invTT.oneStep();

        modelDC = invDC.model();
        modelTT = invTT.model();
        DEBUG save(modelDC, "modelDC-" + toStr(iter) + ".vector");
        DEBUG save(modelTT, "modelTT-" + toStr(iter) + ".vector");
    }

    //** Collect coverage vector for logarithmic (without bounds!) quantities
    RVector covDC(coverageDCtrans(*fDC.jacobian(), 1.0 / invDC.response(), 1.0 / modelDC));
    RVector one(dataTT.size(), 1.0);
    RVector covTT(transMult(*fTT.jacobian(), one));
    save(covTT, "coverageTT.vector");
    save(covDC, "coverageDC.vector");
    
    RVector scovTT(transMult(fTT.constraintsRef(), fTT.constraintsRef() * covTT));
    for (Index i = 0 ; i < scovTT.size() ; i++) scovTT[ i ] = sign(std::abs(scovTT[ i ]));
    save(scovTT, "scoverageTT.vector");

    std::map< std::string, RVector > resultsToShow;
    resultsToShow.insert(std::make_pair("Resistivity/Ohmm" , modelDC));
    resultsToShow.insert(std::make_pair("Velocity/m/s" , 1.0 / modelTT));
//    resultsToShow.insert(std::make_pair("Resistivity(log10)", log10(abs(modelDC) + 1e-16)));
    resultsToShow.insert(std::make_pair("coverageDC", covDC));
    resultsToShow.insert(std::make_pair("coverageTT", covTT));
    resultsToShow.insert(std::make_pair("scoverageTT", scovTT));
    paraDomain.exportVTK("dc_tt.result", resultsToShow);

    std::string pref = "";
    if (noCoupling) pref = "-N-";

    save(modelDC, "modelDC"+ pref+".vector");
    save(RVector(1.0 / modelTT), "modelTT"+ pref+".vector");
//    delete transDataDC;
//    delete transModelDC;
//    delete transDataTT;
//    delete transModelTT;

    return EXIT_SUCCESS;
}
