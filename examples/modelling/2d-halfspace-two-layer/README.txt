Very simple 2d "real world simulation"

24 Electrodes Wenner-alpha array on two layered earth.
Top layer 10 Ohmm with 1m thickness and 100 Ohmm Background.

The shell script way: calc.sh

Files:
    calc.sh -- the main calculation script

    wa24.shm -- configuration for Wenner-Alpha measurement with 24 electrodes
