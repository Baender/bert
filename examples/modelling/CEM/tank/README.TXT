Example 1 (experimental tank) from R�cker&G�nther (2011),
idea after tank experiment by Bechtold et al. (2012)

See paper for details. 

References:
Bechtold, M., Vanderborgth, J., Weiherm�ller, J., Herbst, M., G�nther,T., Ippisch, O., Kasteel, R., & Vereecken, H. (2012): Upward transport in a 3-D heterogeneous laboratory soil due to evaporation. Vadose Zone Journal, doi:10.2136/vzj2011.0066.

R�cker, C. & G�nther, T. (2011) The simulation of finite ERT electrodes using the complete electrode model. Geophysics, 76, F227.