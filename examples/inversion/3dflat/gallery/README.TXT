bert->Examples->Inversion->3dFlat->Gallery
###############################################

This example was friendly provided by the University of Mining and Technology, Freiberg.
It is a quite small 3d survey over a known mining gallery that is used for dewatering of the underground mines.
A grid of 14x9 electrodes was installed and dipole-dipole measurements were applied on all x- and y-profiles.

Inversion can be run with standard options using
invertNew3d gallery.dat > inv.cfg
and refined with other options (try it out).
As a result we see how the shallow gallery shows as highly resistive body crossing the measuring area.

