---GENERELLES-----------------------------------------------------------------------

pygimli/pybert Voraussetzung oder add-on? 
    => im Moment ohne=basic features, mit=advanced => im Tutorial py-Features ausweisen

letzte Schritte bis 2.0
-----------------------  
    
    = Nur noch ein Installer ... nie mehr 1 updaten (poly-Scripte nach bert/bin):
        - polyFlatWorld, polyFreeWorld, polyCreateCylinderModel

    = Installer f�r alle Plattformen (W32,W64,Mac64) incl. pygimli + BERT1
        = Test 64bit bert mit 32bit pygimli (und libbert)

    * Tests mit allen BERT1 und Migration Guide Beispielen
    
    * voller Funktionsumfang von BERT1
        =TT Sparsematrix (Paper-Beispiel)
            dokumentieren (SENSMATMAXMEMSIZE f�r Berechnen unabh. von sparse)
                          (SENSMATDROPTOL beim Einlesen -> schreiben+lesen erzwingen
                          in bert-script SENSMATMAXMEM exportieren wenn DROPTOL)
        = BERTTHREADS/SENSMATDROPTOL/SENSMATMAXMEM aus .bertrc exportieren vor Aufruf
            sensitivity sparsing testen

    * Dokumentation (migration guide) und erstes Tutorial auf echtem 2.0 stand

    * Timelapse Pr�prozessing (Generierung einer BMAT aus TIMESTEPS durch dcedit)

    * Datenfile gem�� Unified Data Format Spezifikation (2d-topo im Anhang)
        dcedit rollt Topo ab oder macht aus 3D koord. 2D koord

    * Script:
        ? PARAMESH und PARAGEOMETRY auswerten (bisher exklusiv) f�r z.B. pyGIMLi-PARAMESH und shell-PARAGEOMETRY
        
        ? ein paar Targets (domain, para etc.) klarer bezeichnen

        = bert version oder --version

        Aufr�umen von tempor�ren und permanenten Datein
            ? Darstellung meshParaDomain.bms und mesh/meshParaDomain.bms oder Vermeidung des letzteren

warten (auf 2.x) kann (nicht muss): 
-----------------------------------

    * Linux64 binaries (rpm RedHat/Suse!, deb Debian/Ubuntu)
        - make bindist autotools nachschauen
    
    * speicherverbrauchsanalyse (allgemein, sparse)

    * InversionsManager:
        
        bessere Timelapse bzw. mehr Optionen, xyz-Positionen
    
    * Regul�re Netze ohne Script (erweiterung PolyCreateWorld), 

    * erweiterte Sensor-Token-Liste (x y z[h d])
        . beliebige Abstandseinheiten x/mm x/m x/cm
   
    * verbessertes IP: 
        Einheiten (�/mrad/mV/%)
        time domain IP
        time lapse IP
    
    Stand bei Unabh�ngigkeit von BERT1/dcfemlib? 2.1, 2.5, 3.0 ??

        -Notwendig daf�r:
            - polytools@pygimli
            - Ersatz f�r dctriangle und andere binaries
            
    bertNew*
        optimierte Einstellungen und bessere Gliederung
        mehr sinnvolle Erg�nzungen
        PRIMDX aus Datenfile in bertNew* absch�tzen?
        PARADEPTH in CFG (kommentiert oder nicht?)

    bei 2dtopo Topo nach au�en extrapolieren (sonst Knick an erster/letzer Elektrode)

    2d paraDomain (optional) 3-Punkt Untergrenze (80%, 130%, 80% Paradepth)

    3D-Interface-files (wie 2D Punktlisten, die in 3d trianguliert und als faces hinzu gef�gt werden)
        Example daf�r

    Bug bei gro�en (z.B. UTM) Koordinaten (3D): paradepth, createParaMesh

---DOKUMENTATION-----------------------------------------

Markierung am Rand was BERT2 ist und wof�r man pygimli/pybert braucht

�bersicht, �ber verschiedene Wege Netz(e) zu definieren
    auto(create*), PARADOMAIN (PLC,SH,PY), PARAMESH (BMS,SH,PY)

Vollst�ndiges Tutorial, Migration Guide gemergt und Howto (s. doc/todo.txt)

Modelling with BERT 2 (Matze)
        CEM modelling examples aus dem Paper
        
---HOWTO-------------------------------------------------

= 2d profiles in 3d topography (Kilmore 2010 Datensatz)
    a) 2d inversion -> 3d vtk (fence diagram)
    b) 3d inversion

= echtes hybrides Dreiecks und Vierecksnetz (Borkum)

---SCRIPTE-----------------------------------------------

createParaMesh.py for DIMENSION=3 

? Absch�tzung versch. Parameter schon bei bertNew* (PARADEPTH,PRIMDX)

= globales .bertrc file einlesen

= Target mkpdf auch ohne Display (Webinversion), (renderer auf Agg)

pdf output (zumindest auf Windows) 10 mal so gro� wie eps->pdf 
	=> auf Linux probieren
	=> mit anderen Renderern Probieren
	=> im Zweifelsfall mit epstopdf umwandeln => Transparenz weg

= mkpdf target f�r Timelapse (from matplotlib.backends.backend_pdf import PdfPages)
	=> s.a. pdf filegroe�e

= show macht auch pytripatch bei timelapse

immer vtk file von Elektroden machen
    Bei 3D in meshs automatisch elec.xyz.vtk bauen (koords *dat oder *topo)

irref�hrende Bezeichnung von domain (Paramesh) und para (secmesh) bzw. SECP2MESH
    => SEC->FWD

? KEEPPRIMPOT speichert auch pot(primmesh) und interpolate interpoliert nur (py-app?)

bertInfo/Summary �hnlich wie unter BERT 1

= Erzeugung von regul�ren Netzen (�. .3d Format?) o. automatisch (DX=)

poly-Scripte f�r h�ufige Geometrien
    polyCreateCylinderModel $FILE $DIA $HEIGTH (x/y zentriert, z-top=0)
    polyCreateBoxModel $FILE $X $Y $Z (x_left,y_front,z_top=0) + $DX f�r regul�r

= TT check ob TOPOPOINTS/TOPOPOLY/REGIONFILE/RESOLUTIONFILE etc. existieren

Variable CONTOURS und =USECOVERAGE, LINCOLOR f�r pytripatch

ordentliche Behandlung der Tokens h/d/z und Topographie am Fileende
    nicht direkt mit alten meshtools m�glich -> schrittweise ersetzung
    entweder dcedit in bertNew oder pytool

---pyGIMLi/pyBERT-----------------------------------------------------------------------

ordentliche Datendarstellung wie bei DC2dInvRes (Thomas!)

= pytripatch colorbar auf Basis von 3% Interquartilen (-i)

pytripatch -C plottet linearen colorbar malt aber logarithmisch (colorbar.py:121 geht schief)
           -C ohne --cMin/cMax geht gar nicht

---DCINV----------------------------------------------------------------------------

Check mit setK( RVector( 1 )) (Inversion von Impedanzen)

= referenceModel als globaler switch und option?

? verbessertes RECALC-Schema? (z.B. 1.-n. und dann alle m?)

= save error bei robustData und SAVEALOT

--- timelapse ---

eigenes target f�r nur timelapse

    1. bleibt in dcinv, 2. ein py-script, 3. verschiedene py f�r versch. Strategien

�bersicht �ber alle Strategien in wenige Parameter gepackt

    A) Reference-based
        Constrain Model against: i) background model ii) last time step iii) zero
                                    (default)            (switch needed)     (fullTLAbsModel)
        identische Arrays:
        (bei LaBrecque: Correct misfit mit i) oder ii)

    B) ratio oder quickmap (erfordert anderes Preprocessing)

    C) fully discretized

---DCMOD----------------------------------------------------------------------------

= -m interpoliert mit switch (-R) direkt auf verfeinertes Netz (meshSec.bms entf�llt)

? Switch KEEPPRIMPOT f�r Abspeichern des orig. Primpotentials (primaryPot/primPot.bms) nur 2D

---DCEDIT---------------------------------------------------------------------------

auch s2d/s3d, tx0, txt(resecs etc.), einlesen? (oder besser in pyBERT-BERTA)

Topographie-Teil in Datenfile (2d) - abrollen durch dcedit

error estimation based on normal reciprocals (NBINS, ADDERRORLEVEL)

dcedit filtert Timelapse-Daten vor, evtl. sogar mit Zeitstempel (interpolation)

IP in �/mrad oder nur so -> abspeichern in (m)rad und auch Behandeln so

--filter ist nicht wirklich verst�ndlich bzw. nicht kompatibel mit DataContainer.filter() 
    m�sste eher --remove hei�en, vl. braucht man beides

= iperr lesen/schreiben und mit einbeziehen

= delete bad data e.g. dups (A=1 B=1), inf, nan, A= <0 etc.

---EXAMPLES / WIKI / QUESTS------------------------------------------------------------

negative apparent resistivities (JungMinLee2009)
    Task: Create benchmark example 
    Test: CEM-body vs. p1 vs. p2 vs. SingRemoval 
    Quest: Can adaptive meshing improve our life here?


---INSTALLER/DISTRIBUTION-----------------------------------------------------------

bin�rere Linux-Installer f�r dcfemlib

.bertrc file mit PYTRIPATCH=pytripatch.py
  SENSMATMAXMEM, BERTTHREADS abfragen

polyFlatWorld in extra-dist (wie geht das bei cmake?)

?dcmatlab (bert2dpost) files in die distribution?

(= abgehakt, =TT to test, ? Wei� nich)
