// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <dcfemlib.h>
#include <longoptions.h>
#include <iostream>

#include <domain2d.h>
#include <domain3d.h>

using namespace std;
using namespace DCFEMLib;

int main( int argc, char * argv[] ){

  LongOptionsList lOpt;
  lOpt.setLastArg("[-x:y:z:r:] file");
  lOpt.setDescription( (string) "Add nodes for local refinement in VIP[-m 99] vicinity, distance [-x, -y, -z, -r]" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "dx", required_argument, 'x', "Refinement distance in x direction [ 0.0 ]" );
  lOpt.insert( "dy", required_argument, 'y', "Refinement distance in y direction [ 0.0 ]" );
  lOpt.insert( "dz", required_argument, 'z', "Refinement distance in z direction [ 0.0 ]" );
  lOpt.insert( "dr", required_argument, 'r', "Refinement distance absolute to domain center [ 0.0 ]" );

  bool help = false, verbose = false;
  double dx = 0.0, dy = 0.0, dz = 0.0, dr = 0.0;
  int option_char = 0, option_index = 0, tracedepth = 0;
  while ( ( option_char = getopt_long( argc, argv, "?hv"
				       "x:"
				       "y:"
				       "z:"
				       "r:", lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    case 'x': dx = atof( optarg ); break;
    case 'y': dy = atof( optarg ); break;
    case 'z': dz = atof( optarg ); break;
    case 'r': dr = atof( optarg ); break;
    default : cerr << "default value not defined" <<  (char)option_char << endl;
    }
  }
  if ( argc < 2 ) { lOpt.printHelp( argv[0] ); return 1; }

  string worldName( argv[ argc - 1] );
  string inputSuffix = ".poly";

  string worldFileName( worldName.substr( 0, worldName.rfind( inputSuffix ) ) + inputSuffix );
  string outputBase( worldName.substr( 0, worldName.rfind( inputSuffix ) ) );

  int dimension = findDomainDimension( worldFileName );

  BaseMesh *world;

  switch ( dimension ){
  case 2:
    world = new Domain2D();
    if ( dz != 0.0 && dy == 0.0 ) dy = dz;
    break;
  case 3:
    world = new Domain3D();
    break;
  default: return -1;
  }


  world->load( worldFileName );
  RealPos centralPoint( averagePosition( world->nodePositions() ) );
  if ( verbose ) cout << centralPoint << endl;
  vector < RealPos > electrodeList( world->positions( world->getAllMarkedNodes( -99 ) ) );

  vector < RealPos > refList( world->positions( world->getAllMarkedNodes( -999 ) ) );

  for ( uint i = 0; i < refList.size(); i++ ) electrodeList.push_back( refList[ i ] );

  //** for CEM compatibility
  for ( int i = 0; i < world->nodeCount(); i ++ ){
    if ( world->node( i ).marker() < -9999 ) electrodeList.push_back( world->node( i ).pos() );
  }

  for ( uint i = 0; i < electrodeList.size(); i ++ ){
    if ( fabs( dr ) > 0.0 ) {
      if ( ( electrodeList[ i ] - centralPoint ).abs() < fabs( dr ) ){
        world->createVIP( electrodeList[ i ] +
            RealPos( (electrodeList[ 0 ]- Line( electrodeList[ 0 ], centralPoint ).lineAt( dr )).abs()/2.0, 0.0, 0.0 ) ) ;
      } else {
        world->createVIP( Line( electrodeList[ i ], centralPoint ).lineAt( dr 
                        / Line( electrodeList[ i ], centralPoint ).length() ) );
//        world->createVIP( Line( electrodeList[ i ], centralPoint ).lineAt( dr ) 
//                        / Line( electrodeList[ i ], centralPoint ).length() );
      }

    } else if ( fabs( dx ) > 0 || fabs( dy ) > 0 || fabs( dz ) > 0 ){
      world->createVIP( electrodeList[ i ] + RealPos( dx, dy, dz ) );
    }
  }
  world->save( worldFileName );

  return 0;
}

/*
$Log: polyRefineVIPS.cpp,v $
Revision 1.14  2010/01/18 11:27:32  thomas
fixed bug in polyRefineVIPS for -r option (PRIMDX_R)

Revision 1.13  2009/04/02 16:38:42  carsten
*** empty log message ***

Revision 1.12  2009/04/02 16:33:15  carsten
*** empty log message ***

Revision 1.11  2008/11/21 07:51:57  thomas
only changes in documentation

Revision 1.10  2008/04/09 14:59:50  carsten
*** empty log message ***

Revision 1.9  2007/11/13 14:29:56  carsten
*** empty log message ***

Revision 1.7  2007/10/02 16:33:11  carsten
*** empty log message ***

Revision 1.6  2007/09/23 21:32:40  carsten
*** empty log message ***

Revision 1.5  2006/03/09 23:37:55  carsten
*** empty log message ***

Revision 1.4  2006/01/22 21:02:06  carsten
*** empty log message ***

Revision 1.3  2005/10/30 18:53:04  carsten
*** empty log message ***

Revision 1.2  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.1  2005/05/31 10:50:04  carsten
*** empty log message ***

*/
