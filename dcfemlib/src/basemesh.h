// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef BASEMESH__H
#define BASEMESH__H BASEMESH__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "elements.h"
using namespace MyMesh;

#include "myvec/vector.h"
#include <map>
#include <vector>
using namespace std;

namespace MyMesh{

class BaseMesh;
class Mesh2D;
class Mesh3D;

DLLEXPORT void findMinMaxVals( const BaseMesh & mesh,
		     double & xmin, double & xmax, double & ymin,
		     double & ymax, double & zmin, double & zmax, bool reset = true );
DLLEXPORT void findMinMaxVals( const vector < RealPos >  & posVector,
		     double & xmin, double & xmax, double & ymin,
		     double & ymax, double & zmin, double & zmax, bool reset = true );


vector < Node * > commonNodes( const BaseElement & e0, const BaseElement & e1 );
double area( const vector < Node * > & nodes );
struct lesserPos{
  bool operator()( const RealPos & a, const RealPos & b ) const {
    if ( fabs( a[ 0 ] - b[ 0 ] ) < TOLERANCE ){
      if ( fabs( a[ 1 ] - b[ 1 ] ) < TOLERANCE ){
        return a[ 2 ] < b[ 2 ];
      } else {
        return a[ 1 ] < b[ 1 ];
      }
    } else {
      return a[ 0 ] < b[ 0 ];
    }
  }
};

struct lesserpNode{
  bool operator()( const Node * a, const Node * b ) const {
    if ( fabs( a->pos()[ 0 ] - b->pos()[ 0 ] ) < TOLERANCE ){
      if ( fabs( a->pos()[ 1 ] - b->pos()[ 1 ] ) < TOLERANCE ){
        return a->pos()[ 2 ] < b->pos()[ 2 ];
      } else {
        return a->pos()[ 1 ] < b->pos()[ 1 ];
      }
    } else {
      return a->pos()[ 0 ] < b->pos()[ 0 ];
    }
  }
};


//! A mesh container.
/*! Its only a mesh container with an list of nodes, a list of edges and a list of triangles
  and serveral function i.e. save, load, meshQualityImprovment. */
class DLLEXPORT BaseMesh{
public:
  /*! Creates an empty Mesh */
  BaseMesh( );
  /*! Constructs a copy of mesh */
  BaseMesh( const BaseMesh & mesh );

  BaseMesh( const string & fname );

  /* Descructs the mesh and all included objects */
  virtual ~BaseMesh();

  /*! Assigns a shallow copy of mesh to this BaseMesh and returns a reference to this container */
  virtual BaseMesh & operator = ( const BaseMesh & mesh );

  /*! Clears all included Lists and objects */
  virtual void clear();

  virtual int dim() const = 0;

  inline Node * createNode( const Node & a ){
    return createNode_( a.x(), a.y(), a.z(), a.id(), a.marker() );
  }
  inline Node * createNode( const RealPos & pos, int id = -1, int marker = 0 ){
    return createNode_( pos.x(), pos.y(), pos.z(), id, marker );
  }
  inline Node * createNode( double x, double y, int id = -1, int marker = 0 ){
    return createNode_( x, y, 0.0, id, marker);
  }
  inline Node * createNode( double x, double y, double z, int id = -1, int marker = 0){
    return createNode_( x, y, z, id, marker);
  }

  virtual BaseElement * createCell( const BaseElement & cell );
  virtual BaseElement * createBoundary( const BaseElement & bound );

  virtual BaseElement * createCellFromNodeIdxVector( const vector < long > & idx, double attribute ) = 0;
  virtual BaseElement * createBoundaryFromNodeIdxVector( const vector < long > & idx, int marker ) = 0;

  //  virtual Edge * createEdge( Edge & a );
//   virtual Edge * createEdge( Node & a, Node & b, int id = -1, int marker = 0);
//   virtual Edge3 * createEdge3( Node & a, Node & b, Node & c, int id = -1, int marker = 0);

  //  virtual BaseElement * createCell( const vector < Node * > & pNodeVector, double attribute );


//   virtual BaseElement * createTriangle( Node & a, Node & b, Node & c, int id = -1, float attribute = 0){
//     cerr << WHERE_AM_I << " to overload" << endl; return NULL; }
//   virtual BaseElement * createTetrahedron( Node & a, Node & b, Node & c, Node & d, int id = -1,
// 					   float attribute = 0){
//     cerr << WHERE_AM_I << " to overload" << endl; return NULL; }
//   virtual BaseElement * createQuadrangle( Node & a, Node & b, Node & c, Node & d,
// 					  int id = -1, float attribute = 0){
//     cerr << WHERE_AM_I << " to overload" << endl; return NULL; }

  virtual RegionMarker * createMarker( const RegionMarker & m );
  virtual RegionMarker * createRegionMarker( double x = 0.0, double y = 0.0, double attribute = 0.0, double dx = 0.0 );
  virtual RegionMarker * createRegionMarker( const RealPos & pos, double attribute = 0.0, double dx = 0.0 );
  virtual void insertRegionMarker( const RealPos & pos, double attribute, double dx = 0.0 ){
    regions_.push_back( new RegionMarker( pos, attribute, dx ) );
  }
  virtual void createRegion( const RegionMarker & region ){
    insertRegionMarker( region.pos(), region.attribute(), region.dx() );
  }
  virtual void createRegion( const RealPos & pos, double attribute, double dx = 0.0  ){
    insertRegionMarker( pos, attribute, dx );
  }
  virtual void insertRegionMarker( const RegionMarker & marker ){ regions_.push_back( new RegionMarker( marker ) ); }

  virtual void createNeighoursInfos(){ TO_IMPL; }

  void deleteAllCells();

  int regionCount() const { return regions_.size(); }
  RegionMarker region( int i ) const { return * regions_[ i ]; }
  RegionMarker & region( int i ) { return * regions_[ i ]; }
  RegionMarker * pRegion( int i ) { return regions_[ i ]; }

  VectorpRegionMarker regions() const { return regions_; }
  VectorpRegionMarker & regions() { return regions_; }

  /*! Saves the whole Mesh to files: �fbody�.n, �fbody�.s, �fbody�.e \n
    �fbody�.n:  Node.id    Node.x   Node.y  Node.z   Node.marker \n
    �fbody�.s:  Edge.id    Edge.nodea    Edge.nodeb   Edge.leftTriangle   Edge.rightTriangle   Edge.marker \n
   �fbody�.e:  Triangle.id Triangle.node1 Triangle.node2 Triangle.node3 Triangle.edge1 Triangle.edge2 Triangle.edge3 Triangle.attribute */
  double area() const {
    double area = 0.0;
    for ( int i = 0; i < cellCount(); i ++ ) area += cell( i ).area();
    return area;
  }

  virtual int save( const string & fbody, const string & style, IOFormat format = Ascii );
  virtual int save( const string & fbody, IOFormat format ){ return save( fbody, "DCFEMLib", format ); }
  virtual int save( const string & fbody ){ return save( fbody, Ascii ); }
  /*! Load the whole Mesh from files with specific input style. (at the moment, Grummp, and dcfemmesh styles supported.\n
    To load a:\n
    DCFEMMesh-mesh -- style = 'DCFEMLib' or 'm' or 'M'
    Grummp-mesh -- style = 'Grummp' or 'g' or 'G'
   */
  virtual int load( const string & fbody, const string & style, IOFormat format = Ascii );
  virtual int load( const string & fbody, IOFormat format ){ return load( fbody, "DCFEMLib", format ); }
  virtual int load( const string & fbody, double snapTol = 1e-12 ){ return load( fbody, Ascii ); }

  /*! Load the whole Mesh from file: �fbody�.mesh. GRUMP output style*/
  virtual int loadGrummpMesh(const string & fbody );

  virtual int saveDCFEMLibMesh(const string & fbody );
  virtual int loadDCFEMLibMesh(const string & fbody );

  virtual int saveBinary( const string & fname );
  virtual int loadBinary( const string & fname );

  virtual int saveNodes( const string & fname );
  virtual int loadNodes( const string & fname );

  virtual int saveCells( const string & fname );
  virtual int loadCells( const string & fname );

  virtual int saveBoundaries( const string & fname );
  virtual int loadBoundaries( const string & fname );

  /*! Returns the number of nodes */
  inline int nodeCount() const { return pNodeVector_.size(); }

  /*! Returns the reference to Node No. i (0, .., size-1) */
  DLLEXPORT Node & node( int i ) const;

  /*! Returns the number of cells. */
  virtual int cellCount() const { return pCellVector_.size(); }

  BaseElement & cell( int i ) const { return element( i ); }
  BaseElement & cell( int i ){ return element( i ); }
  /*! Returns the reference to Triangle No. i (0, .., size-1) */
  DLLEXPORT BaseElement & element( int i ) const;

  /*! Returns the number of boundarys. */
  virtual int boundaryCount() const { return pBoundaryVector_.size(); };

  BaseElement & boundary( int i ) const { return * pBoundaryVector_[ i ]; }
  BaseElement & boundary( int i ){ return * pBoundaryVector_[ i ]; }

  /*! Returns the number of markers. */
  int markerCount( ) const { return regions_.size(); }
  RegionMarker & marker( int i ) const { return *regions_[ i ]; }
  RegionMarker & marker( int i ) { return *regions_[ i ]; }

  void initMat( double mat[ 4 ][ 4 ] );

   virtual void transform( double mat[ 4 ][ 4 ] ){
        //std::cout << "BaseMesh::transform " << std::endl;
        for ( int i = 0, imax = nodeCount(); i < imax; i ++ ) node( i ).pos().transform( mat );
    }


  void translate( double tx, double ty, double tz );
  void translate( const RealPos & pos ) { translate( pos.x(), pos.y(), pos.z() ); }

  void rotate( double phix, double phiy, double phiz, const RealPos & relative );

  void rotate( double phix, double phiy, double phiz ){
    rotate( phix, phiy, phiz, RealPos( 0.0, 0.0, 0.0 ) );
  }
  void scale( double sx, double sy = 0.0, double sz = 0.0);


  /* forward declarations for domain stuff */
  virtual void merge( const BaseMesh & subMesh, double tolerance = TOLERANCE,bool check = true ){ TO_IMPL; }

  virtual Node * createVIP( const RealPos & pos, int marker, double tolerance = TOLERANCE ){ TO_IMPL; return NULL; }

  virtual Node * createVIP( const RealPos & pos, double tolerance = TOLERANCE ){ TO_IMPL; return NULL; }

  virtual void createHole( const RealPos & pos ){ TO_IMPL; }


//   /*! Returns the edgeCount */
//   inline int edgeCount() const { return _edgesVector.size(); }
//   /*! Returns the reference to Edge No. i (0, .., size-1) */
//  BaseElement & edge( int i ) const;


  vector< Node > nodes( const vector < int > & index ) const {
    vector < Node > nodes;
    for ( size_t i = 0, imax = index.size(); i < imax; i ++ ) nodes.push_back( node( index[ i ] ) );
    return nodes;
  }

  vector< RealPos > positions( const vector < int > & index ) const {
    vector < RealPos > positions;
    for ( size_t i = 0, imax = index.size(); i < imax; i ++ ) {
      if ( index[ i ] > -1 && index[ i ] < nodeCount() ){
	positions.push_back( node( index[ i ] ).pos() );
      } else {
	cout << WHERE_AM_I << " index " << index[ i ] << " out of range " << endl;
      }
    }
    return positions;
  }

  vector< RealPos > nodePositions() const {
    vector < RealPos > positions;
    for ( size_t i = 0, imax = nodeCount(); i < imax; i ++ ) {
      positions.push_back( node( i ).pos() );
    }
    return positions;
  }

 /*! Counts list of Nodes, Edges and Triangles and sets new id�s, this is nessesary befor saving, and refinement to delete holes at these lists*/
  virtual void recount();
  /*! Returns 1 if NodeCount = 0. */
  virtual int empty() { return pNodeVector_.empty(); }

  /*! Deletes All Nodes */
  void deleteAllNodes();
  virtual void deleteNode( Node & node );
  virtual void deleteEdge( Edge & edge );
  virtual void eraseEdge( Edge & edge );
  virtual void eraseElement( BaseElement & element );

  virtual void improveMeshQuality( bool nodeMoving, bool edgeSwapping, int smoothFunktion, int smoothIteration){
    TO_IMPL
  }

  /*! Sets the List of for refinement selected Triangles and initialise all Subinfos*/
  void setRefineSelectList(const VectorpElements & refine);
  /*! Select Triangle triangle for refinement */
  inline void addElementToRefineList( BaseElement & element){ refineList.push_back( & element ); }
  inline void clearRefineList( ){ refineList.clear(); }

  /*! Refines the selected Triangles. See \ref setRefineSelectList and returns the new Mesh2d */
  void refine( const VectorpElements & refine );
  virtual int refine() = 0;
  //  void findMiddlePos( Node & nodeA, Node & nodeB, double & x, double & z );

  virtual int createRefined( const BaseMesh & mesh, const vector < int > & cellIdx ) = 0;

  double xmin(){ return worldKoordinate( 1 ); }
  double xmax(){ return worldKoordinate( 2 ); }
  double ymin(){ return worldKoordinate( 3 ); }
  double ymax(){ return worldKoordinate( 4 ); }
  double zmin(){ return worldKoordinate( 5 ); }
  double zmax(){ return worldKoordinate( 6 ); }

  double xMin(){ return worldKoordinate( 1 ); }
  double xMax(){ return worldKoordinate( 2 ); }
  double yMin(){ return worldKoordinate( 3 ); }
  double yMax(){ return worldKoordinate( 4 ); }
  double zMin(){ return worldKoordinate( 5 ); }
  double zMax(){ return worldKoordinate( 6 ); }

  RealPos middlePos(){ return averagePosition( pNodeVector_ ); }
  BaseElement & findNearestCell( const RealPos & pos ) const;

  double worldKoordinate( int pos );

//   SetpNodes collectNeighbours( Node & node );

  virtual void showInfos() = 0;

//   int refined(){ return _refined.size(); }
//   int subMeshStartNode(unsigned int i) { if (i < _refined.size()); return _refined[i]; }
//   vector<int> refinedVector() { return _refined; }
//   vector<int> refinedVector() const { return _refined; }

  virtual BaseElement * findBoundary( BaseElement & n0, BaseElement & n1 ) = 0;
 // virtual BaseElement & findBoundary( const BaseElement & n0, const BaseElement & n1 ) const = 0;
//   virtual BaseElement & findBoundary( const BaseElement & n0, const BaseElement & n1 ) const {
//     //    cout << WHERE_AM_I << endl;
//     return findBoundary( n0, n1 );
//   }

  DLLEXPORT SetpCells findNeighbours( BaseElement & cell ) const;

  VectorpNodes pNodesVector() const { return pNodeVector_; }
  VectorpNodes & pNodesVector() { return pNodeVector_; }

  virtual void qualityCheck(){ TO_IMPL }

  virtual void refineBadTriangle( Triangle & triangle ){ cerr << WHERE_AM_I << triangle.id(); }

  DLLEXPORT vector < int > getAllMarkedNodes( int marker ) const ;
  DLLEXPORT vector < int > cellIndex() const ;

  RVector allAttributes( ) const {
    RVector attributes( cellCount() );
    for ( int i = 0; i < cellCount(); i ++ ) attributes[ i ] = cell( i ).attribute();
    return attributes;
  }
  void setAllAttributes( const RVector & attributes ){
    if ( (int)attributes.size() == cellCount() ){
      for ( int i = 0; i < cellCount(); i ++ ) cell(i).setAttribute( attributes[i] );
    } else {
      cerr << WHERE_AM_I << " attributes.size() don't match cellCount() " <<
	attributes.size() << " != " << cellCount() << endl;
    }
  }
  virtual void mapCellAttributes( const map < float, float > & aMap );
  virtual void mapCellAttributes( const vector< int > & cellMap, const RVector & attributeMap, double defaultVal);
  virtual void mapBoundaryConditions( const map < float, float > & bMap );

  void fillEmptyCells( vector < BaseElement * > & empty );

  bool verbose() const { return verbose_; }
  void setVerbose( bool verbose ){ verbose_ = verbose;}

  void findWorldKoordinates();

  virtual void findSubMeshByAttribute( const BaseMesh & mesh, double from, double to = -1 );
  virtual void findSubMeshByIdx( const BaseMesh & mesh, vector < int > & idxList );
  virtual void createH2Mesh( const BaseMesh & mesh ) = 0;
  virtual void createP2Mesh( const BaseMesh & mesh ) = 0;

  virtual int saveOOGL_Off( const string & outputname, double shrink ){ TO_IMPL return 0;}
  virtual int saveINRIAMeshFile( const string & outputname, const RVector & data, bool logScaleData ){ TO_IMPL return 0;}

  int saveVTKUnstructured( const string & outputname, const RVector & data = RVector(0), bool logScaleData = false );
  int saveVTKUnstructured( const string & fbody, const vector < RealPos > & vecData );
  virtual int saveVTK( const string & outputname, const RVector & data = RVector(0), bool logScaleData = false ){
  return saveVTKUnstructured( outputname, data, logScaleData ); }

  virtual int saveVTK( const string & outputname, const vector < double > & data, bool logScaleData = false ){
    //** shame on me :(
    RVector dat( data.size() );
    for ( int i = 0; i <dat.size(); i ++ ) dat[ i ] = data[ i ];

  return saveVTKUnstructured( outputname, dat, logScaleData );
  }

  virtual int saveGMVFile( const string & outputname ){ TO_IMPL return 0; }

  vector < BaseElement * > findpCellAttribute( double from, double to ) const {
    if ( (to + 1.0 ) < TOLERANCE ) to = 1e99;

    vector < BaseElement * > vCell;
    for ( int i = 0; i < cellCount(); i ++ ) {
      if ( cell( i ).attribute() >= from && cell( i ).attribute() <= to ) vCell.push_back( pCellVector_[ i ] );
    }
    return vCell;
  }

protected:

  virtual Node * createNode_( double x, double y, double z, int id, int marker );

  virtual void refineBadTriangles( double angle ){ angle = 0.0;}

//   void divideTriangle(Mesh2D & mesh, Triangle & triangle);

//   virtual void divideTriangle( Triangle & triangle );
//   virtual void bisectTriangle( Triangle & triangle );

  void clearSubInfos();

//   int loadEdges( const string & fname );
//   int saveEdges( const string & fname );
//   virtual int loadElements( const string & fname ) = 0;
//   virtual int saveElements( const string & fname ) = 0;

  VectorpNodes pNodeVector_;
  VectorpElements pCellVector_;
  VectorpElements pBoundaryVector_;

  //std::set< Node *, lesserpNode > nodeSet_;

  VectorpElements refineList;
  VectorpNodes _edgeSubNode;
  vector< int > _elementRefineStatus;

  double xmin_, xmax_, zmin_, zmax_, ymin_, ymax_;
  int _isworldmatrixknown;

  bool verbose_;

  VectorpRegionMarker regions_;
};
} // namespace MyMesh

#endif // BASEMESH__H

/*
$Log: basemesh.h,v $
Revision 1.48  2011/08/30 14:09:51  carsten
fix memory problem within mesh-copy

Revision 1.47  2010/01/31 10:49:54  carsten
*** empty log message ***

Revision 1.46  2008/12/11 15:14:13  carsten
*** empty log message ***

Revision 1.45  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.44  2008/04/15 16:53:04  carsten
*** empty log message ***

Revision 1.43  2007/10/04 07:21:59  thomas
BERT + polytools codeblocks (dllexport)

Revision 1.42  2007/09/13 15:47:14  carsten
*** empty log message ***

Revision 1.41  2007/09/11 17:28:58  carsten
*** empty log message ***

Revision 1.40  2007/04/04 16:53:54  carsten
*** empty log message ***

Revision 1.39  2007/02/21 19:44:05  carsten
*** empty log message ***

Revision 1.38  2006/11/08 19:53:24  carsten
*** empty log message ***

Revision 1.37  2006/09/22 11:05:28  carsten
*** empty log message ***

Revision 1.36  2006/07/28 16:24:51  carsten
*** empty log message ***

Revision 1.35  2006/06/06 09:41:49  carsten
*** empty log message ***

Revision 1.34  2006/05/08 16:25:59  carsten
*** empty log message ***

Revision 1.33  2006/01/22 21:02:06  carsten
*** empty log message ***

Revision 1.32  2005/10/28 14:35:55  carsten
*** empty log message ***

Revision 1.31  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.30  2005/10/20 14:33:49  carsten
*** empty log message ***

Revision 1.29  2005/10/18 16:29:56  carsten
*** empty log message ***

Revision 1.28  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.27  2005/10/04 18:05:51  carsten
*** empty log message ***

Revision 1.26  2005/09/23 11:24:41  carsten
*** empty log message ***

Revision 1.25  2005/08/12 16:43:20  carsten
*** empty log message ***

Revision 1.24  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.23  2005/07/14 15:03:37  carsten
*** empty log message ***

Revision 1.22  2005/06/28 16:38:16  carsten
*** empty log message ***

Revision 1.21  2005/06/02 10:36:42  carsten
*** empty log message ***

Revision 1.20  2005/05/31 10:50:04  carsten
*** empty log message ***

Revision 1.19  2005/05/03 18:46:46  carsten
*** empty log message ***

Revision 1.18  2005/04/12 17:05:50  carsten
*** empty log message ***

Revision 1.17  2005/04/12 11:53:54  carsten
*** empty log message ***

Revision 1.16  2005/04/11 13:31:23  carsten
*** empty log message ***

Revision 1.15  2005/04/11 13:12:35  carsten
*** empty log message ***

Revision 1.14  2005/04/07 11:41:08  carsten
*** empty log message ***

Revision 1.13  2005/03/20 22:51:59  carsten
*** empty log message ***

Revision 1.12  2005/03/10 16:04:38  carsten
*** empty log message ***

Revision 1.11  2005/02/16 19:36:38  carsten
*** empty log message ***

Revision 1.10  2005/02/09 15:11:43  carsten
*** empty log message ***

Revision 1.9  2005/01/13 21:12:15  carsten
*** empty log message ***

Revision 1.8  2005/01/13 20:10:42  carsten
*** empty log message ***

Revision 1.7  2005/01/12 20:32:40  carsten
*** empty log message ***

Revision 1.6  2005/01/07 15:27:30  carsten
*** empty log message ***

Revision 1.5  2005/01/07 15:23:05  carsten
*** empty log message ***

Revision 1.4  2005/01/06 19:10:52  carsten
*** empty log message ***

Revision 1.3  2005/01/06 19:00:32  carsten
*** empty log message ***

Revision 1.2  2005/01/06 17:07:42  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
