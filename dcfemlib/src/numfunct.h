// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef NUMFUNCT__H
#define NUMFUNCT__H NUMFUNCT__H

typedef double real_;

const real_ PI_=3.141592653589793238462643383279502884197169399375105820974944592308;

#include "dcfemlib.h"
#include "stlmatrix.h"

// #include <cmath>
// #include <cassert>
// #include <iostream>

#include <vector>
// #include <string>

using namespace std;

class IntegrateTerm;

int sign( double a );

double round( double val, unsigned int count );

template < class T > void swapVal( T & a, T & b ){
  T tmp( a );
  a = b; b = tmp;
}

double fakultaet( int max );

class IntegrateTerm{
public:
  IntegrateTerm( double factor, int i, int j, int xPot = 0, int yPot = 0, int zPot = 0, int dim = 2 )
    : factor_( factor ), iKoord_( i ), jKoord_(j),
      xPot_(xPot), yPot_(yPot), zPot_( zPot ), dim_( dim ) {
    zaehler_ = 0.0;
    nenner_ = 0.0;
    calc();
  }
  virtual ~IntegrateTerm(){}

  virtual void calc(){
    zaehler_ = fakultaet( xPot_ ) * fakultaet( yPot_ ) * fakultaet( zPot_ );
    switch ( dim_ ){
    case 2: nenner_ = fakultaet( xPot_ + yPot_ + 2); break;
    case 3: nenner_ = fakultaet( xPot_ + yPot_ + zPot_ + 3); break;
    }
    zaehler_ *= factor_;
    //    cout << zaehler_ << "\t" << nenner_ << endl;

//     if ( fabs( ( (double)nenner_ / (double)zaehler_ ) -
// 	       rint( (double)nenner_/(double)zaehler_) ) < 1e-12){
//       nenner_ /= zaehler_;
//       zaehler_ = 1;
//     }
  }

  void show(){
    cout << zaehler_ << "/" << nenner_ << endl;
  }
  double val(){ return zaehler_/nenner_; }

  int i(){ return iKoord_; }
  int j(){ return jKoord_; }
  double zaehler(){ return zaehler_; }
  double nenner(){ return nenner_; }
  void setDim( int dim ){ dim_ = dim; }

protected:
  double factor_;
  int iKoord_;
  int jKoord_;
  int xPot_;
  int yPot_;
  int zPot_;
  int dim_;
  double zaehler_;
  double nenner_;
};

DLLEXPORT MyVec::STLMatrix TriangleLinearA( );
DLLEXPORT MyVec::STLMatrix TriangleQuadA( );
DLLEXPORT MyVec::STLMatrix TetrahedronLinearA( );
DLLEXPORT MyVec::STLMatrix TetrahedronQuadA( );

DLLEXPORT vector < IntegrateTerm > intLinTriangle_u_xi2(); // u_xi^2
DLLEXPORT vector < IntegrateTerm > intLinTriangle_u_eta2(); // u_eta^2
DLLEXPORT vector < IntegrateTerm > intLinTriangle_u_xi_u_eta(); // u_xi u_eta
DLLEXPORT vector < IntegrateTerm > intLinTriangle_u2(); // u * u
DLLEXPORT vector < IntegrateTerm > intQuadTriangle_u_xi2();// u_xi^2
DLLEXPORT vector < IntegrateTerm > intQuadTriangle_u_eta2();  // u_eta^2
DLLEXPORT vector < IntegrateTerm > intQuadTriangle_u_xi_u_eta(); // u_xi_u_eta
DLLEXPORT vector < IntegrateTerm > intQuadTriangle_u2();  // u * u
DLLEXPORT vector < IntegrateTerm > intLinTetrahedron_u_xi2(); // u_xi^2
DLLEXPORT vector < IntegrateTerm > intLinTetrahedron_u_eta2(); // u_eta^2
DLLEXPORT vector < IntegrateTerm > intLinTetrahedron_u_zeta2(); // u_zeta^2
DLLEXPORT vector < IntegrateTerm > intLinTetrahedron_u_xi_u_eta(); // u_xi u_eta
DLLEXPORT vector < IntegrateTerm > intLinTetrahedron_u_xi_u_zeta(); // u_xi u_eta
DLLEXPORT vector < IntegrateTerm > intLinTetrahedron_u_eta_u_zeta(); // u_xi u_eta
DLLEXPORT vector < IntegrateTerm > intLinTetrahedron_u2();
DLLEXPORT vector < IntegrateTerm > intQuadTetrahedron_u_xi2();
DLLEXPORT vector < IntegrateTerm > intQuadTetrahedron_u_eta2();
DLLEXPORT vector < IntegrateTerm > intQuadTetrahedron_u_zeta2();
DLLEXPORT vector < IntegrateTerm > intQuadTetrahedron_u_xi_u_eta();
DLLEXPORT vector < IntegrateTerm > intQuadTetrahedron_u_xi_u_zeta();
DLLEXPORT vector < IntegrateTerm > intQuadTetrahedron_u_eta_u_zeta();
DLLEXPORT vector < IntegrateTerm > intQuadTetrahedron_u2();

DLLEXPORT MyVec::STLMatrix createMatrix( size_t dim, vector < IntegrateTerm > terms );
IntegrateTerm analyseTerm( const string & termstring, int dim = 2);
vector < IntegrateTerm > parseString( const string & mathematicaOut, int dim = 2);

DLLEXPORT double besselI0( double x );
DLLEXPORT double besselI1( double x );
DLLEXPORT double besselK0( double x );
DLLEXPORT double besselK1( double x );

real_ BesselI0(real_ x);
real_ BesselI1(real_ x);
real_ BesselK0(real_ x);
real_ BesselK1(real_ x);
real_ Bessel(real_ x);
real_ GaussLegendreQuadraturAbscissas( int n, int k );
real_ GaussLegendreQuadraturWeight( int n, int k );
real_ GaussLaguerreQuadraturAbscissas( int n, int k );
real_ GaussLaguerreQuadraturWeight( int n, int k );

void GaussLegendre( double x1, double x2, size_t n, vector < double > & x, vector < double > & w );
void GaussLaguerre( size_t n, vector < double > & x, vector < double > & w );

inline real_ _hypot(real_ a, real_ b) { return(sqrt((a*a) + (b*b))); }
real_ determinant2x2( real_ a, real_ b, real_ c, real_ d );
DLLEXPORT real_ det2( real_ a, real_ b, real_ c, real_ d );
DLLEXPORT double det( const MyVec::STLMatrix & A );
MyVec::STLMatrix inv( const MyVec::STLMatrix & A );

real_ determinant( real_ * A[], int dim = -1 );

/*! Converts a double degree value to double radient.*/
DLLEXPORT double degToRad( double deg );
/*! Converts a double radian value to double degree.*/
DLLEXPORT double radToDeg( double rad );

#endif

/*
$Log: numfunct.h,v $
Revision 1.7  2007/10/04 07:21:59  thomas
BERT + polytools codeblocks (dllexport)

Revision 1.6  2007/04/04 16:53:54  carsten
*** empty log message ***

Revision 1.5  2006/11/03 13:25:40  carsten
*** empty log message ***

Revision 1.4  2005/10/09 18:54:44  carsten
*** empty log message ***

Revision 1.3  2005/10/07 17:19:49  carsten
*** empty log message ***

Revision 1.2  2005/01/13 21:12:15  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
