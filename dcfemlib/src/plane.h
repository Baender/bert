// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef PLANE__H
#define PLANE__H PLANE__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "elements.h"
#include "line.h"

namespace MyMesh{

//
/*! */
class Plane{
public:
  /*! Default constructor. Construct a x-y plan, p0(0.0, 0.0, 0.0), p1 (1.0, 0.0, 0.0), p2 (0.0, 1.0, 0.0) */
  Plane( );
  /*! Construct a plane based on Hessian normal form norm * x = -p.*/
  Plane( const RealPos & norm, const double p );
  /*! Construct a plane based on his unit nomal vector norm and a base position x0.*/
  Plane( const RealPos & norm, const RealPos & x0 );
  /*! Construct a plane based on 3 real positions in R^3, respectivly the in parameterized style.*/
  Plane( const RealPos & p0, const RealPos & p1, const RealPos & p2 );
  /*! Construct a plane based on his general equation */
  Plane( double a, double b, double c, double d );
  /*! Copyconstructor.*/
  Plane( const Plane & plane );
  /*! Default destructor*/
  ~Plane();

  /*! */
  Plane & operator = ( const Plane & plane );
  /*! */
  bool operator == ( const Plane & plane );
  /*! */
  bool operator != ( const Plane & plane );
  /*! */
  bool compare( const Plane & plane, double tol = TOLERANCE );

  /*! Returns the distance between the Point pos and this plane.*/
  double distance( const RealPos & pos ) { return norm_.scalar( pos ) - d_ ; }

  /*! Returns true (1) if the Point pos touches this plane respectivly the distance is lower than the tolerance tol. Else this method returns 0.*/
  int touch ( const RealPos & pos, double tol = TOLERANCE );

  /*! Returns the straight line of intersection. Are booth planes parallel the returned line is invalid.*/
  Line intersect( const Plane & plane, double tol = TOLERANCE );
  /*! Returns the point of intersection between this plane and the Line line. Are booth parallel the returned RealPos is invalid.*/
  RealPos intersect( const Line & line, double tol = TOLERANCE );

  /*! Returns the unit vector of this plain, respectivly to position x0. Length of the unit vector is one.*/
  RealPos norm( ) const { return norm_; }
  /*! Returns the orign if the unit vector.*/
  RealPos x0( ) const { return norm_ * d_; }
  /*! Distance between thor orign(0,0,0) and this plane.*/
  double d() const { return d_; }

  /*!Check if the plane spans a valid R3 space. It must lineaer independend*/
  bool checkValidity();
  /*! The straight line is not valid if p0_ == p1_ or it is not initialized by default constructor.*/
  bool valid() const { return valid_; }
  /*! */
  void setValid( bool valid ) { valid_ = valid; }

protected:
  RealPos norm_;
  double d_;
  bool valid_;
};

ostream & operator << ( ostream & str, const Plane & p );

} //namespace MyMesh

#endif // PLANE__H

/*
$Log: plane.h,v $
Revision 1.2  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.1  2005/01/12 20:32:40  carsten
*** empty log message ***

*/
