// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include "longoptions.h"

void LongOptionsList::insert( const string & name, int hasArg, char key, const string & help ){
  this->push_back( LongOption( name, hasArg, key, help) );
}

void LongOptionsList::printHelp( const string & main ){
  cout << "Usage: " << main << " [options] " << lastArg_ << endl;
  cout << "Description: " << description_ << endl;
  cout << "Options:" << endl;

  for ( list< LongOption >::iterator it = this->begin(); it != this->end(); it++){
    cout << "  -" << (char)(*it).key()
	 << ",  --" << (*it).name()
	 << "\t\t: " << (*it).help() << endl;
  }
  cout << endl;
}

struct option * LongOptionsList::long_options(){
  delete[ ] opts_;
  opts_ = new struct option[ this->size() + 1 ];
  int count = 0;
  for ( list< LongOption >::iterator it = this->begin(); it != this->end(); it++){
    opts_[ count ].name = (char*)(*it).name().c_str();
    opts_[ count ].has_arg = (*it).hasArg();
    opts_[ count ].val = (*it).key();
    opts_[ count ].flag = 0;
    count ++;
  }
  opts_[ count ].name = 0;
  opts_[ count ].has_arg = 0;
  opts_[ count ].val = 0;
  opts_[ count ].flag = 0;
  return opts_;
}

