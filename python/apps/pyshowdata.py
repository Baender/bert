#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This program is part of pybert
Visit http://www.resistivity.net for further information or the latest version.
"""
import sys

from os import system, path

try:
    import pygimli as pg
    import pygimli.mplviewer
except ImportError:
    import traceback
    traceback.print_exc(file=sys.stdout)
    sys.stderr.write("ERROR: cannot import the library 'pygimli'."
        "Ensure that pygimli is in your PYTHONPATH")

try:
    import pybert as pb
except ImportError:
    import traceback
    traceback.print_exc(file=sys.stdout)
    sys.stderr.write("ERROR: cannot import the library 'pybert'. "
        "Ensure that pybert is in your PYTHONPATH")

import pybert.data
from pybert.importer import importData

import matplotlib.pyplot as plt
import numpy as np

def loadAndShowBERT(options, axes=None):
    """
    """
    data = importData(options.dataFileName,
                      verbose=options.verbose)

    if options.verbose:
        print(data)

    if data.size() == 0:
        raise BaseException("Data size is 0 for file:", options.dataFileName)

    if options.reciprocity:
        tmp = pg.Vector(data['a'])
        data['a'] = data['m']
        data['m'] = tmp
        tmp = pg.Vector(data['b'])
        data['b'] = data['n']
        data['n'] = tmp
        data.sortSensorsIndex()
        # data.save('rec', 'a b m n rhoa u i')

    try:
        vals = data(options.token)

        if min(vals) == max(vals):
            print(("Warning!", options.token, "min == max = ", min(vals)))

        if options.token == 'err':
            vals *= 100
            options.label = "Error [%]"
    except Exception as e:
        print(e)
        print(("try import file: ", options.token))
        vals = pg.RVector(options.token)

    if options.verbose:
        print(("Token: ", options.token))
        print(("min max ", pg.min(vals), pg.max(vals)))

    if options.verbose:
        data.showInfos()

    axes = pb.show(data, vals=vals, schemeName=options.scheme, axes=axes,
                  colorBar=True, linear=options.cLin,
                  cMin=options.cMin, cMax=options.cMax,
                  label=options.label)
    return axes

#def loadAndShowBERT(...):

def loadAndShowTraveltime(options, axes=None):
    """
    """
    from pygimli.mplviewer import drawTravelTimeData

    data = pg.DataContainer()
    data.registerSensorIndex("s")
    data.registerSensorIndex("g")
    data.load(options.dataFileName)
    data.sortSensorsIndex()
    #data.save(options.dataFileName + '.sort')

    if options.token:
        try:
            print(("cannot load ", options.token))
            data.set('t', pg.RVector(options.token))
        except:
            print(("cannot load ", options.token))
            pass

    drawTravelTimeData(axes, data)
    return axes
# def loadAndShowTraveltime(...):

def main(argv):

    import argparse

    parser = argparse.ArgumentParser(description = "usage: %prog [options] *.dat|*.shm|*.ohm")

    parser.add_argument("-v", "--verbose", dest = "verbose", action = "store_true"
                            , help = "Be verbose.", default = False)
    parser.add_argument("-S", "--silent", dest = "silent", action = "store_true"
                            , help = "Set viewer silent mode without gui (only create an image)", default = False)
    parser.add_argument("-o", "--output", dest = "outFileName"
                            , help = "Filename for the resulting image. Suffix define the fileformat (.pdf|.png|.svg)"
                            , metavar = "File")
    parser.add_argument("-d", "--data", dest = "token"
                            , help = "Data token or file name for Ascii RVector that will be displayed."
                            , metavar = "String"
                            , default = "rhoa")

    parser.add_argument("--cMin", dest = "cMin"
                            , help = "minimum colour", type = float)
    parser.add_argument("--cMax", dest = "cMax"
                            , help = "maximum colour", type = float)

    parser.add_argument("--cLin", dest = "cLin", action = "store_true"
                            , help = "Set linear colorbar norm.", default = False)

    parser.add_argument("--label", dest="label"
                            , help="label to be displayed", metavar="String"
                            , default = r"Apparent resistivity [$\Omega$m]")
    parser.add_argument("--xLabel", dest="xlabel",
                        metavar="String", default=None,
                        help="Label to the x-axis.")
    parser.add_argument("--yLabel", dest="ylabel",
                        metavar="String", default=None,
                        help="Label to the y-axis.")

    parser.add_argument("--title", dest="title", default ="", help="draw title")

    parser.add_argument("--scheme", dest="scheme", default ="auto", help="tmp hack until autodetect draw pseudosection scheme")

    parser.add_argument("--patchData", dest = "patchData", default = False, 
                        action="store_true"
                        , help = "Draw patch figure instead of simple matrix.")
    parser.add_argument("-R", "--showReciprocity", dest="reciprocity", default=False, 
                        action="store_true", 
                        help="Draw reciproc configurations if they are exist.")

    parser.add_argument('dataFileName')

    options = parser.parse_args()

    if options.verbose: print(options)

    fig = plt.figure()

    #strange!!!!!! plt.figure() overwrites our locale settings to the system default.
    pg.checkAndFixLocaleDecimal_point(options.verbose)

    axes = fig.add_subplot(1, 1, 1)

    if options.token != 'rhoa' and options.label is r"Apparent resistivity [$\Omega$m]":
        options.label = options.token

    if '.sgt' in options.dataFileName:
        loadAndShowTraveltime(options, axes=axes)
    else:
        axes,_ = loadAndShowBERT(options, axes=axes)

    if options.title:
        axes.set_title(options.title)
    if options.xlabel:
        axes.set_xlabel(options.xlabel)
    if options.ylabel:
        axes.set_ylabel(options.ylabel)

    if options.outFileName:
        if options.verbose:
            print(("writing: ", options.outFileName))
        fig = axes.figure

        # scale = 0.5
        # fig.set_size_inches(float(1024 *scale)/fig.get_dpi(),
        #                     float(1453 *scale)/fig.get_dpi())

        (fileBaseName, fileExtension) = path.splitext(options.outFileName)

        if (fileExtension == '.svg'):
            plt.savefig(options.outFileName)
        elif (fileExtension == '.pdf'):
            plt.savefig(options.outFileName, bbox_inches='tight')
        elif (fileExtension == '.png'):
            plt.savefig(options.outFileName)
        else:
            assert False, ('format %s unknown. (available(svg, png, pdf))' % fileExtension)

    if (not options.silent):
        plt.show()

    #pylab.show()

if __name__ == "__main__":
    main(sys.argv[1:])
